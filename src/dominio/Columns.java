package dominio;

import java.awt.Rectangle;

public class Columns {
    
    public int x;
    public int y;
    public int width;
    public int height;
    public String orientation;
    
    public Columns(){
        
    }
    
    public Columns(int x, int y,int width, int height, String orientation){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height =height;
        this.orientation = orientation;
    }
    
    public int getX(){
        return x;
    }
    
    public int getY(){
        return y;
    }
    
    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }
    
    public String getOrientation(){
        return orientation;
    }
    
    public Rectangle getRectangle(){
        Rectangle rectangle = new Rectangle(this.x,this.y,this.width,this.height);
        return rectangle;
    }
    
    
    public void setX(int x){
        this.x = x;
    }
    
    public void setY(int y){
        this.y = y;
    }
    
    public void setWidth(int width){
        this.width = width;
    }
    
    public void setHeight(int height){
        this.height = height;
    }
    
    public void setOrientation(String orientation){
        this.orientation = orientation;
    }
   
}
