
package dominio;

public class Score {
       int score;
    String name;
    
    public Score(String name, int score){
        this.name = name;
        this.score = score;
    }
    
    public int getScore(){
        return score;
    }
    
    public String getName(){
        return name;
    }
    
    public void setScore(int score){
        this.score = score;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    @Override
    public String toString(){
        return this.name + " " + this.score;
    }
}
