package util;

import dominio.Score;
import java.util.Comparator;

/** Comparator engine
 * <p> This is the class we use to sort our 
 * object Score.
 * We sort two times : </p>
 * <ul>
 * <li> We start with the score </li>
 * <li> And if the score are equal (return 0) 
 * we compare with name</li>
 * </ul>
 */

public class SortByScore implements Comparator<Score>{
    @Override
    public int compare(Score a, Score b) 
    {       
         Integer x1 = ((Score) a).getScore();
         Integer x2 = ((Score) b).getScore();
         int sComp = x2.compareTo(x1);
         
         if(sComp != 0){
             return sComp;
         }
         
         String s1 = ((Score) a).getName();
         String s2 = ((Score) b).getName();
         return s1.compareTo(s2);
    }
}
