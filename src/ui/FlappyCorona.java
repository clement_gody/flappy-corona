package ui;

import dominio.Bird;
import dominio.Columns;
import dominio.Score;
import io.ScoreIO;
import util.Util;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * <b>Main Class of Flappy Corona</b>
 * @author Clément Gody
 * @version 1.0
 
 */

public class FlappyCorona implements ActionListener{
    
    
    public static FlappyCorona flappyCorona;

    public final int WIDTH = 800, HEIGHT = 800;

    public JWindows windows;
    public Bird bird;
    public ArrayList<Columns> columns;

    public Random rand;

    public boolean gameOver, started;
    public int click, yMotion, score;
    public int speed = 4;
    public String name;

    private JButton btnEasy;
    private JButton btnMedium;
    private JButton btnHard;
    private final JButton btnScore;
    public JTextField txtName;
    
    /** 
     * Method main which run the class FlappyCorona
     * @param args arguments of the method main
     */
    public static void main(String[] args) {
        flappyCorona = new FlappyCorona();
    }
    
    
    /**This is one intern class which
     * create our frame and panels that 
     * than user interact with.
     */
    public FlappyCorona(){
        JFrame jframe = new JFrame();
        Timer timer = new Timer(20, this);

        JPanel pnlName = new JPanel();
        JPanel pnlGame = new JPanel();
        JPanel pnlLevel = new JPanel();
        
        /*=================Jpanel Name + hight score=================*/
        JLabel lblName = new JLabel("Choose your virus name :");
        txtName = new JTextField("Corona", 8);
        btnScore = new JButton("Best Virus");
        btnScore.addMouseListener(new MouseAdapter()
        {   
            @Override
            public void mouseClicked(MouseEvent e)
            {
                TreeMap<Score, Integer> scores = ScoreIO.importScore();
                Collection bestScores = scores.keySet();
                List list = new LinkedList();
                list.addAll(bestScores);
                StringBuilder sb = new StringBuilder();
                for(int i=0; i<10; i++)
                {
                    sb.append(list.get(i));
                    sb.append("\n");
                }
                javax.swing.JOptionPane.showMessageDialog(jframe, sb);
            }    

        });
        pnlName.add(lblName);
        pnlName.add(txtName);
        pnlName.add(btnScore);
        /*=================Jpanel Name + hight score=================*/
        
        /*=================       Jpanel Game      =================*/
        windows = new JWindows();
        rand = new Random();
        windows.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
        windows.addMouseListener(new MouseAdapter()
        {   
            @Override
            public void mouseClicked(MouseEvent e)
            {
                jump();
            }
        });
        pnlGame.add(windows);
        bird = new Bird((WIDTH / 2) - 150, HEIGHT / 2 -100 ,60,60);
        columns = new ArrayList<>();

        this.addColumn(true);
        this.addColumn(true);
        this.addColumn(true);
        this.addColumn(true);
        
        /*=================       Jpanel Game      =================*/
        
        /*=================       Jpanel Level     =================*/
        
        JLabel lblLevel = new JLabel("Choose your level :");

        btnEasy = new JButton("Endemic");
        btnMedium = new JButton("Epidemic");
        btnHard = new JButton("Pandemic");
        btnEasy.setEnabled(false);
        btnMedium.setEnabled(true);
        btnHard.setEnabled(true);
        btnEasy.addMouseListener(new MouseAdapter()
        {   
            @Override
            public void mouseClicked(MouseEvent e)
            {
                speed = 4;
                btnEasy.setEnabled(false);
                btnMedium.setEnabled(true);
                btnHard.setEnabled(true);
            }
        });
        btnMedium.addMouseListener(new MouseAdapter()
        {   
            @Override
            public void mouseClicked(MouseEvent e)
            {
                speed = 6;
                btnEasy.setEnabled(true);
                btnMedium.setEnabled(false);
                btnHard.setEnabled(true);
            }
        });
        btnHard.addMouseListener(new MouseAdapter()
        {   
            @Override
            public void mouseClicked(MouseEvent e)
            {
                speed = 8;
                btnEasy.setEnabled(true);
                btnMedium.setEnabled(true);
                btnHard.setEnabled(false);
            }
        });
        pnlLevel.add(lblLevel);
        pnlLevel.add(btnEasy);
        pnlLevel.add(btnMedium);
        pnlLevel.add(btnHard);

        /*=================       Jpanel Level     =================*/

        jframe.add(pnlName, BorderLayout.NORTH);
        jframe.add(pnlLevel, BorderLayout.SOUTH);
        jframe.add(pnlGame, BorderLayout.CENTER);
        //jframe.add(windows);
        jframe.setTitle("Flappy Corona");
        jframe.setFocusable(true);
        jframe.setSize(WIDTH, HEIGHT);
        jframe.setResizable(true);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        timer.start();
    }
    
    /**
     * This method paint every graphics element 
     * of our Game Panel except columns which
     * needs more specify actions.
     * @param g graphics parameter
     */
    public void repaint(Graphics g) {
        
        g.drawImage(Util.loadImage("images/background.png"), 0,0,WIDTH,HEIGHT, null);
        g.drawImage(Util.loadImage("images/foreground.png"), 0,0,WIDTH,HEIGHT, null);
        g.drawImage(Util.loadImage("images/virus.png"),bird.getX() ,bird.getY(),bird.getWidth(),bird.getHeight(), null); 
        
        for(Columns column:columns)
        {
            paintColumn(g,column);
        }
        
        g.setColor(Color.white);
        g.setFont(new Font("Arial", 1, 100));
        if (!started)
        {
            g.drawString("Click to start!", 75, HEIGHT / 2 - 50);
        }
        if(gameOver)
        {
            g.drawString("Game Over!", 100, HEIGHT / 2 - 50);
        }
        if (!gameOver && started)
        {
            g.drawString(String.valueOf(score), WIDTH / 2 - 25, 100);
        }
    }
    
    /**
     * This method create and add columns in our 
     * ArrayList we created in the class FlappyCorona()
     * @param start True we add columns False we don't
     */
    public void addColumn(boolean start){
        int space = 300;
        int width = 100;
        int height = 50 + rand.nextInt(300);

        if(start)
        {
            columns.add(new Columns(WIDTH + width + columns.size() * 300, HEIGHT - height - 120, width, height, "images/north.png"));
            columns.add(new Columns(WIDTH + width + (columns.size() - 1) * 300, 0, width, HEIGHT - height - space, "images/south.png"));
        }else
        {
            columns.add(new Columns(columns.get(columns.size() - 1).x + 600, HEIGHT - height - 120, width, height, "images/north.png"));
            columns.add(new Columns(columns.get(columns.size() - 1).x, 0, width, HEIGHT - height - space, "images/south.png"));
        }
    }
    
    /**
     * Method paintColumn
     * It use to transform our object to graphic compoments.
     * @param g graphic parameter
     * @param column object for our constructor Columns
     */
    public void paintColumn(Graphics g, Columns column){
        g.drawImage(Util.loadImage(column.getOrientation()),column.getX(),column.getY(),column.getWidth(),column.getHeight(), null);
    }
    
    /**
     * Method Jump 
     * This method is used when the user click in the game
     * panel to fly up the virus
     */
    public void jump(){
        if (gameOver)
        {   
            name = txtName.getText();
            ScoreIO.exportScore(new Score(name, score));

            bird = new Bird((WIDTH / 2) - 150, HEIGHT / 2 -100 ,60,60);
            columns.clear();
            yMotion = 0;
            score = 0;

            addColumn(true);
            addColumn(true);
            addColumn(true);
            addColumn(true);

            gameOver = false;
        }

        if (!started)
        {
            started = true;
        }else if (!gameOver)
        {
            if (yMotion > 0)
            {
                yMotion = 0;
            }
            yMotion -= 10;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e){
        click ++;

        if(started){
            for(int i =0; i < columns.size(); i++){
            Columns column = columns.get(i);
            column.x -= speed;
            }
            if(click % 2 == 0 && yMotion < 15)
            {
                yMotion += 2;
            }
            for(int i =0; i < columns.size(); i++)
            {
                Columns column = columns.get(i);
                column.x -= speed;
                if (column.x + column.width < 0)
                {
                    columns.remove(column);
                    if (column.y == 0)
                    {           
                        addColumn(false);
                    }
                }
            }
            bird.y+= yMotion;

            for(Columns column : columns){
            Rectangle rectC = column.getRectangle();
            Rectangle rectB = bird.getRectangle();

                if(column.y == 0 && bird.x + bird.width / 2 > column.x + column.width / 2 - (speed) && bird.x + bird.width / 2 < column.x + column.width / 2 + (speed))
                {
                    score++;
                }
                if(rectC.intersects(rectB))
                {
                    gameOver = true;
                    bird.x = column.x - bird.width;
                }  
            }   
            if (bird.y > HEIGHT - 120 || bird.y < 0)
            {
                gameOver = true;
            }
            if(bird.y + yMotion >= HEIGHT-120)
            {
                bird.y = HEIGHT - 120- bird.height;
            }
        }    
        windows.repaint();
    }
}

