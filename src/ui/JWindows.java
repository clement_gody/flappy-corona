package ui;

import java.awt.Graphics;
import javax.swing.JPanel;

public class JWindows extends JPanel {
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        FlappyCorona.flappyCorona.repaint(g);
    }
}
