package io;

import dominio.Score;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeMap;
import util.SortByScore;
/** Score IO class
 * This class is use to export or import our data 
 * in one csv document.
 * @author Clement Gody
 */
public class ScoreIO {
    
    /** 
     * Method export
     * @param score it's an object with one String 
     * and one int
     */
     public static void exportScore(Score score){
         try
        {
            FileWriter fw = new FileWriter("data/scores.csv", true);
            PrintWriter pw = new PrintWriter(fw); 
            pw.println(score);
            pw.close();
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }
    
     /** 
      * Method import
      * @return TreeMap 
      * <p> We use a TreeMap because we need to sort our 
      * element to show to the user only the 10 best score</p>
      * @see SortByScore
      */
    public static TreeMap<Score, Integer> importScore(){
         TreeMap<Score, Integer> scores = new TreeMap<>(new SortByScore());
        try
        {
           FileReader fr = new FileReader("data/scores.csv");
           BufferedReader br = new BufferedReader(fr);
           String linea = null;
           int i = 0;
           while((linea = br.readLine()) != null)
           {
                String s[] = linea.split(" ");
                String name = s[0];
                int score = Integer.parseInt(s[1].trim());
                scores.put(new Score(name,score), i);
                i++;
           }

        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
        return scores;
    }
}
